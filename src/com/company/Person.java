package com.company;

public class Person {
    String name;
    int age;
    double salary;
    double height;
    double weight;
    public Person( String name,
            int age,
            double salary,
            double height,
            double weight){
        this.name=name;
        this.age=age;
        this.height=height;
        this.weight=weight;
        this.salary=salary;
    }
}
